Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: MATE Tweak
Upstream-Contact: Martin Wimpress <code@ubuntu-mate.org>
Source: https://github.com/ubuntu-mate/mate-tweak

Files: mate-tweak
       data/*.1
       data/mate-tweak.desktop
       data/mate-tweak.ui
Copyright: 2007-2014, Clement Lefebvre <root@linuxmint.com>
           2015-2021, Martin Wimpress <code@ubuntu-mate.org>
License: GPL-2+

Files: .gitignore
       .github/FUNDING.yml
       .tx/config
       marco-wrapper
       util/mate-tweak-helper
       data/marco-xrender.desktop
       data/marco-glx.desktop
       data/marco-xr_glx_hybrid.desktop
       data/marco-no-composite.desktop
       data/org.mate.mate-tweak.policy
       README.md
Copyright: 2015-2021, Martin Wimpress <code@ubuntu-mate.org>
License: GPL-2+

Files: po/*
Copyright:
 Adriano Torres Azevedo <adrianotorres78@gmail.com>
 Akerbeltz <fios@akerbeltz.org>
 Alan Mortensen <alanmortensen.am@gmail.com>
 AlephAlpha <alephalpha911@gmail.com>
 Cédric VALMARY (Tot en òc) <cvalmary@yahoo.fr>
 Cedrik Heckenbach <cedrik.heckenbach@gmail.com>
 Clement Lefebvre <root@linuxmint.com>
 Cyril Weller <cyril.yc.weller@gmail.com>
 Dawid de Jager <dawid.dejager@gmail.com>
 Dhruv <meetsangvikar@gmail.com>
 Diogo Oliveira <diogodesigns@gmail.com>
 dragnadh <dragnadh@gmail.com>
 enolp <enolp@softastur.org>
 Erieck Saputra <erick.1383@gmail.com>
 Fazwan Fazil <takaizu91@gmail.com>
 Flaviu <flaviu@gmx.com>
 Gearóid  Ó Maelearcaidh <gearoid@omaelearcaidh.com>
 gudeta <gudbog@gmail.com>
 Harald H. <haarektrans@gmail.com>
 Hoysala T'swamy <jrandomnerd@gmail.com>
 Isidro Pisa <isidro@utils.info>
 Ismael Omar <ismaelhssn@gmail.com>
 Ivica  Kolić <ikoli@yahoo.com>
 박정규(Jung Kyu Park) <bagjunggyu@gmail.com>
 Juris Daikteris <juris.daikteris@gmail.com>
 KAMI <kami911@gmail.com>
 Kenan Dervišević <kenan@dkenan.com>
 KOSAL (David Salvo) <slinkeepy@gmail.com>
 Louis <louis77@mac.com>
 Martin Wimpress <code@flexion.org>
 Michael Moroni <michael.moroni@openmailbox.org>
 Miguel Anxo Bouzada <mbouzada@gmail.com>
 Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
 mzs.112000 <mzs.112000@gmail.com>
 Nicat Məmmədov <n1c4t97@gmail.com>
 Niroj Bista <nirooj56@outlook.com>
 Ondřej Kolín <ondrej.kolin@gmail.com>
 Pierluigi Zavaroni <zavamail@yahoo.com>
 Piotr Strębski <strebski@o2.pl>
 Prabhath Mannapperuma <d.p.mannapperuma@gmail.com>
 Praveen Illa <mail2ipn@gmail.com>
 Rhoslyn Prys <rprys@yahoo.com>
 Rockworld <sumoisrock@gmail.com>
 Saki <a4e484b@hotmail.com>
 Sakis Proto <sakis.proto@gmail.com>
 segray <segray@tut.by>
 Sipan Roj <sipanroj@live.com>
 Steven Liao <stevenliao0119@yahoo.com.tw>
 Sveinn í Felli <sv1@fellsnet.is>
 Tapani <tapman2012@hotmail.com>
 tibbi <tibbbi2@gmail.com>
 tomoe_musashi <hkg.musashi@gmail.com>
 Victor Ibragimov <victor.ibragimov@gmail.com>
 Yaron <sh.yaron@gmail.com>
 Yngve Spjeld Landro <l10n@landro.net>
 Микола Ткач <Stuartlittle1970@gmail.com>
 Саша Петровић <salepetronije@gmail.com>
License: GPL-2+

Files: setup.py
Copyright: 2015, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
           2016, Martin Wimpress <code@flexion.org>
License: GPL-2+

Files: debian/*
Copyright: 2014-2022, Martin Wimpress <code@ubuntu-mate.org>
           2015-2023, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
           2015, Vangelis Mouhtsis <vangelis@gnugr.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.
